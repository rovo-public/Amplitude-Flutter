import 'package:flutter/widgets.dart';
import './time_utils.dart';

class Session with WidgetsBindingObserver {
  factory Session(int timeout, Function enterExitForeground) {
    if (_instance != null) {
      return _instance;
    }
    _instance = Session._internal(timeout, enterExitForeground);
    return _instance;
  }

  Session._internal(this.timeout, this.enterExitForeground) {
    _time = TimeUtils();
    final widgetsBinding = ensureInitialized();
    if (widgetsBinding != null) {
      widgetsBinding.addObserver(this);
    }
  }

  static WidgetsBinding ensureInitialized() {
    if (WidgetsBinding.instance == null)
      WidgetsFlutterBinding();
    return WidgetsBinding.instance;
  }

  @visibleForTesting
  Session.private(TimeUtils time, this.timeout) {
    _time = time;
  }

  static Session _instance;
  TimeUtils _time;
  Function enterExitForeground;

  int timeout;
  int sessionStart;
  int lastActivity;
  bool _inForeground = true;

  void start({int sessionId, int lastActivity}) {
    if (sessionId != null) {
      sessionStart = sessionId;
    } else {
      sessionStart = _time.currentTime();
    }

    if (lastActivity != null) {
      this.lastActivity = lastActivity;
    } else {
      this.lastActivity = sessionStart;
    }
  }

  String getSessionId() {
    return sessionStart?.toString();
  }

  void refresh() {
    final int now = _time.currentTime();
    if (!withinSession(now)) {
      sessionStart = now;
    }
    lastActivity = now;
  }

  void enterBackground() {
    _inForeground = false;
    enterExitForeground(false);
  }

  void exitBackground() {
    _inForeground = true;
    enterExitForeground(true);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive:
      case AppLifecycleState.paused:
//      case AppLifecycleState.suspending:
        enterBackground();
        break;
      case AppLifecycleState.resumed:
        exitBackground();
        break;
      default:
    }
  }

  bool withinSession(int timestamp, {bool ignoreForegroundTracking = false}) {
    if (lastActivity != null && (ignoreForegroundTracking || !_inForeground)) {
      return (timestamp - lastActivity) < timeout;
    }
    return true;
  }

  bool get isInForeground => _inForeground;
}
