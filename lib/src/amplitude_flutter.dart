import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:mobx/mobx.dart';

import 'config.dart';
import 'device_info.dart';
import 'event.dart';
import 'event_buffer.dart';
import 'identify.dart';
import 'revenue.dart';
import 'service_provider.dart';
import 'session.dart';
import 'time_utils.dart';

class AmplitudeFlutter {
  AmplitudeFlutter(String apiKey, [this.config]) {
    config ??= Config();
    provider = ServiceProvider(
        apiKey: apiKey,
        timeout: config.sessionTimeout,
        enterExitForeground: (bool inForeground) {
          debugPrint('[Analytics] in foreground ' + inForeground.toString());
          if (inForeground) {
            startNewSessionIfNeeded(ignoreForegroundTracking: true);
          }
        });
    _init();
  }

  @visibleForTesting
  AmplitudeFlutter.private(this.provider, this.config) {
    _init();
  }

  static const String PREVIOUS_SESSION_ID_KEY = 'previousSessionId';

  Config config;
  ServiceProvider provider;
  DeviceInfo deviceInfo;
  Session session;
  EventBuffer buffer;
  dynamic userId;
  int sessionId = -1;
  int previousSessionId = -1;
  int lastActivityAt = -1;

  /// Set the user id associated with events
  void setUserId(dynamic userId) {
    this.userId = userId;
  }

  /// Log an event
  Future<void> logEvent(
      {@required String name,
      Map<String, dynamic> properties = const <String, String>{}}) async {
    /// Skip session check if logging start session or end session events
    if (properties['outOfSession'] != true) {
      await asyncWhen((_) => session.getSessionId() != null);

      final loggingSessionEvent =
          name == 'session_start' || name == 'session_end';

      if (!loggingSessionEvent) {
        if (!session.isInForeground) {
          startNewSessionIfNeeded();
        } else {
          session.refresh();
          setLastActivity(getSessionTimestamp(session.getSessionId()),
              session.lastActivity);
        }
      }
    }

    debugPrint('[Analytics] $name ${properties.toString()}');

    session.refresh();
    setLastActivity(
        getSessionTimestamp(session.getSessionId()), session.lastActivity);

    if (config.optOut) {
      return Future.value(null);
    }

    final Event event = Event(name,
        sessionId: session.getSessionId() ?? '-1', props: properties)
      ..addProps(await deviceInfo.getPlatformInfo());

    if (userId != null) {
      event.addProp('user_id', userId);
    }

    if (config.product != null) {
      Map eventProps = event.props['event_properties'];
      if (eventProps == null) {
        eventProps = Map<String, dynamic>();
      }
      eventProps['product'] =
          config.product == Product.WorkoutParty ? 'workoutparty' : 'rovo';
      event.addProp('event_properties', eventProps);
    }

    debugPrint('[Analytics] adding event ${event.toPayload().toString()}');
    return buffer.add(event);
  }

  /// Identify the current user
  Future<void> identify(Identify identify,
      {Map<String, dynamic> properties = const <String, dynamic>{}}) async {
    return logEvent(
        name: r'$identify',
        properties: <String, dynamic>{'user_properties': identify.payload}
          ..addAll(properties));
  }

  /// Adds the current user to a group
  Future<void> setGroup(String groupType, dynamic groupValue) async {
    return identify(Identify()..set(groupType, groupValue),
        properties: <String, dynamic>{
          'groups': <String, dynamic>{groupType: groupValue}
        });
  }

  /// Sets properties on a group
  Future<void> groupIdentify(
      String groupType, dynamic groupValue, Identify identify) async {
    return logEvent(name: r'$groupidentify', properties: <String, dynamic>{
      'group_properties': identify.payload,
      'groups': <String, dynamic>{groupType: groupValue}
    });
  }

  /// Log a revenue event
  Future<void> logRevenue(Revenue revenue) async {
    if (revenue.isValid()) {
      return logEvent(
          name: Revenue.EVENT,
          properties: <String, dynamic>{'event_properties': revenue.payload});
    }
  }

  /// Manually flush events in the buffer
  Future<void> flushEvents() => buffer.flush();

  void _init() async {
    deviceInfo = provider.deviceInfo;
    session = provider.session;
    buffer = EventBuffer(provider, config);

    final previousSession = await provider.store.getLastSession();
    if (previousSession != null && previousSession.item1 >= 0) {
      previousSessionId = previousSession.item1;
      sessionId = previousSessionId;
      lastActivityAt = previousSession.item2;
      session.start(sessionId: sessionId, lastActivity: lastActivityAt);
      debugPrint('[Analytics] loading previous session ' +
          sessionId.toString() +
          ' lastActivity ' +
          lastActivityAt.toString());
    }

    debugPrint('[Analytics] starting if needed');
    startNewSessionIfNeeded(ignoreForegroundTracking: true);
  }

  int getSessionTimestamp(String sessionId) {
    try {
      return int.tryParse(session.getSessionId()) ?? -1;
    } catch (e) {
      return -1;
    }
  }

  void startNewSession() {
    // end previous session
    sendSessionEvent('session_end');

    // start new session
    session.start();
    setSession(getSessionTimestamp(session.getSessionId()));
    sendSessionEvent('session_start');
  }

  void sendSessionEvent(String event) {
    if (!inSession) {
      return;
    }

    logEvent(name: event);
  }

  bool startNewSessionIfNeeded({bool ignoreForegroundTracking = false}) {
    final timestamp = TimeUtils().currentTime();
    if (inSession) {
      if (session.withinSession(timestamp,
          ignoreForegroundTracking: ignoreForegroundTracking)) {
        debugPrint('[Analytics] extending session0 ' +
            session.getSessionId().toString() +
            ' lastActivity ' +
            (session.lastActivity ?? 0).toString() +
            ' diff ' +
            (timestamp - (session.lastActivity ?? 0)).toString());
        session.refresh();
        setLastActivity(timestamp, session.lastActivity);
        debugPrint('[Analytics] extending session1 ' +
            session.getSessionId().toString() +
            ' lastActivity ' +
            (session.lastActivity ?? 0).toString() +
            ' diff ' +
            (timestamp - (session.lastActivity ?? 0)).toString());

        return false;
      }

      debugPrint('[Analytics] start session0 ' +
          session.getSessionId().toString() +
          ' lastActivity ' +
          (session.lastActivity ?? 0).toString() +
          ' timestamp ' +
          timestamp.toString() +
          ' diff ' +
          (timestamp - (session.lastActivity ?? 0)).toString());
      startNewSession();
      debugPrint('[Analytics] start session1 ' +
          session.getSessionId().toString() +
          ' lastActivity ' +
          (session.lastActivity ?? 0).toString() +
          ' timestamp ' +
          timestamp.toString() +
          ' diff ' +
          (timestamp - (session.lastActivity ?? 0)).toString());
      return true;
    }

    // no current session - check for previous session
    if (session.withinSession(timestamp,
        ignoreForegroundTracking: ignoreForegroundTracking)) {
      if (previousSessionId == -1) {
        startNewSession();
        debugPrint('[Analytics] start session2 ' +
            session.getSessionId().toString() +
            ' lastActivity ' +
            (session.lastActivity ?? 0).toString() +
            ' diff ' +
            (timestamp - (session.lastActivity ?? 0)).toString());
        return true;
      }

      // extend previous session
      session.refresh();
      setLastActivity(timestamp, session.lastActivity);
      debugPrint('[Analytics] extending session ' +
          session.getSessionId().toString() +
          ' lastActivity ' +
          (session.lastActivity ?? 0).toString() +
          ' diff ' +
          (timestamp - (session.lastActivity ?? 0)).toString());
      return false;
    }

    startNewSession();
    debugPrint('[Analytics] start session3 ' +
        session.getSessionId().toString() +
        ' lastActivity ' +
        (session.lastActivity ?? 0).toString() +
        ' diff ' +
        (timestamp - (session.lastActivity ?? 0)).toString());
    return true;
  }

  bool get inSession => sessionId >= 0;

  void setSession(int timestamp) async {
    sessionId = timestamp;
    previousSessionId = timestamp;
    lastActivityAt = timestamp;
    await provider.store.setSesssion(timestamp, timestamp);
  }

  void setLastActivity(int timestamp, int lastActivityAt) async {
    lastActivityAt = lastActivityAt;
    await provider.store.setSesssion(timestamp, lastActivityAt);
  }
}
